```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: web
  clusterIP: None
  selector:
    app: nginx
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
spec:
  selector:
    matchLabels:
      app: nginx # Label selector that determines which Pods belong to the StatefulSet
                 # Must match spec: template: metadata: labels
  serviceName: "nginx"
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx # Pod template's label selector
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: nginx
        image: gcr.io/google_containers/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: web
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi

```


```
cloud_user_p_965beb@cloudshell:~$ gcloud compute disks list   --project  playground-s-11-76cc0c
NAME                                                             LOCATION           LOCATION_SCOPE  SIZE_GB  TYPE         STATUS
gke-standard-cluster-1-default-pool-4e37a7c1-0d2l                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-4e37a7c1-8vw7                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-4e37a7c1-9146                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-4e37a7c1-d8gw                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-4e37a7c1-jf26                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-default-pool-4e37a7c1-wmjv                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-1-pvc-0100ec4a-ddd1-11e9-b984-42010a940043  asia-southeast1-a  zone            1        pd-standard  READY
gke-standard-cluster-1-pvc-13c625c5-ddd1-11e9-b984-42010a940043  asia-southeast1-a  zone            1        pd-standard  READY
gke-standard-cluster-1-pvc-1fd9f51d-ddd1-11e9-b984-42010a940043  asia-southeast1-a  zone            1        pd-standard  READY

cloud_user_p_965beb@cloudshell:~$ kubectl get po -o wide
NAME    READY   STATUS    RESTARTS   AGE     IP         NODE                                                NOMINATED NODE   READINESS GATES
web-0   1/1     Running   0          5m3s    10.4.4.3   gke-standard-cluster-1-default-pool-4e37a7c1-9146   <none>           <none>
web-1   1/1     Running   0          4m31s   10.4.1.3   gke-standard-cluster-1-default-pool-4e37a7c1-wmjv   <none>           <none>
web-2   1/1     Running   0          4m11s   10.4.5.2   gke-standard-cluster-1-default-pool-4e37a7c1-0d2l   <none>           <none>

cloud_user_p_965beb@cloudshell:~$ kubectl describe sts web
Name:               web
Namespace:          default
CreationTimestamp:  Mon, 23 Sep 2019 15:08:54 +0800
Selector:           app=nginx
Labels:             <none>
Annotations:        <none>
Replicas:           824637473196 desired | 3 total
Update Strategy:    RollingUpdate
  Partition:        824637473544
Pods Status:        3 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        gcr.io/google_containers/nginx-slim:0.8
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:
      /usr/share/nginx/html from www (rw)
  Volumes:  <none>
Volume Claims:
  Name:          www
  StorageClass:
  Labels:        <none>
  Annotations:   <none>
  Capacity:      1Gi
  Access Modes:  [ReadWriteOnce]
Events:
  Type    Reason            Age    From                    Message
  ----    ------            ----   ----                    -------
  Normal  SuccessfulCreate  6m59s  statefulset-controller  create Claim www-web-0 Pod web-0 in StatefulSet web success
  Normal  SuccessfulCreate  6m59s  statefulset-controller  create Pod web-0 in StatefulSet web successful
  Normal  SuccessfulCreate  6m27s  statefulset-controller  create Claim www-web-1 Pod web-1 in StatefulSet web success
  Normal  SuccessfulCreate  6m27s  statefulset-controller  create Pod web-1 in StatefulSet web successful
  Normal  SuccessfulCreate  6m7s   statefulset-controller  create Claim www-web-2 Pod web-2 in StatefulSet web success
  Normal  SuccessfulCreate  6m7s   statefulset-controller  create Pod web-2 in StatefulSet web successful



cloud_user_p_965beb@cloudshell:~$ kubectl get pv,pvc,po
NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM               STORAGECLASS   REASON   AGE
persistentvolume/pvc-0100ec4a-ddd1-11e9-b984-42010a940043   1Gi        RWO            Delete           Bound    default/www-web-0   standard                8m9s
persistentvolume/pvc-13c625c5-ddd1-11e9-b984-42010a940043   1Gi        RWO            Delete           Bound    default/www-web-1   standard                7m38s
persistentvolume/pvc-1fd9f51d-ddd1-11e9-b984-42010a940043   1Gi        RWO            Delete           Bound    default/www-web-2   standard                7m17s

NAME                              STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/www-web-0   Bound    pvc-0100ec4a-ddd1-11e9-b984-42010a940043   1Gi        RWO            standard       8m13s
persistentvolumeclaim/www-web-1   Bound    pvc-13c625c5-ddd1-11e9-b984-42010a940043   1Gi        RWO            standard       7m41s
persistentvolumeclaim/www-web-2   Bound    pvc-1fd9f51d-ddd1-11e9-b984-42010a940043   1Gi        RWO            standard       7m21s

NAME        READY   STATUS    RESTARTS   AGE
pod/web-0   1/1     Running   0          8m13s
pod/web-1   1/1     Running   0          7m41s
pod/web-2   1/1     Running   0          7m21s




++++++++++++++++++++++++++
Region level cluster

cloud_user_p_965beb@cloudshell:~$ kubectl get po,pv,pvc,sc
NAME        READY   STATUS    RESTARTS   AGE
pod/web-0   1/1     Running   0          5m32s
pod/web-1   1/1     Running   0          5m1s
pod/web-2   1/1     Running   0          4m29s
pod/web-3   1/1     Running   0          104s
pod/web-4   1/1     Running   0          88s
pod/web-5   1/1     Running   0          67s

NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM               STORAGECLASS   REASON   AGE
persistentvolume/pvc-553c1902-ddd4-11e9-bc2e-42010a940047   1Gi        RWO            Delete           Bound    default/www-web-3   standard                99s
persistentvolume/pvc-5ed0bbd2-ddd4-11e9-bc2e-42010a940047   1Gi        RWO            Delete           Bound    default/www-web-4   standard                84s
persistentvolume/pvc-6b44acf3-ddd4-11e9-bc2e-42010a940047   1Gi        RWO            Delete           Bound    default/www-web-5   standard                62s
persistentvolume/pvc-cdabc75a-ddd3-11e9-bc2e-42010a940047   1Gi        RWO            Delete           Bound    default/www-web-0   standard                5m27s
persistentvolume/pvc-dfa8d014-ddd3-11e9-bc2e-42010a940047   1Gi        RWO            Delete           Bound    default/www-web-1   standard                4m56s
persistentvolume/pvc-f2b124d9-ddd3-11e9-bc2e-42010a940047   1Gi        RWO            Delete           Bound    default/www-web-2   standard                4m25s

NAME                              STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/www-web-0   Bound    pvc-cdabc75a-ddd3-11e9-bc2e-42010a940047   1Gi        RWO            standard       5m32s
persistentvolumeclaim/www-web-1   Bound    pvc-dfa8d014-ddd3-11e9-bc2e-42010a940047   1Gi        RWO            standard       5m1s
persistentvolumeclaim/www-web-2   Bound    pvc-f2b124d9-ddd3-11e9-bc2e-42010a940047   1Gi        RWO            standard       4m29s
persistentvolumeclaim/www-web-3   Bound    pvc-553c1902-ddd4-11e9-bc2e-42010a940047   1Gi        RWO            standard       104s
persistentvolumeclaim/www-web-4   Bound    pvc-5ed0bbd2-ddd4-11e9-bc2e-42010a940047   1Gi        RWO            standard       88s
persistentvolumeclaim/www-web-5   Bound    pvc-6b44acf3-ddd4-11e9-bc2e-42010a940047   1Gi        RWO            standard       67s

NAME                                             PROVISIONER            AGE
storageclass.storage.k8s.io/standard (default)   kubernetes.io/gce-pd   8m44s
cloud_user_p_965beb@cloudshell:~$ gcloud compute disks list   --project playground-s-11-76cc0c
NAME                                                             LOCATION           LOCATION_SCOPE  SIZE_GB  TYPE         STATUS
gke-standard-cluster-2-default-pool-52d5d3f8-2m20                asia-southeast1-c  zone            100      pd-standard  READY
gke-standard-cluster-2-default-pool-52d5d3f8-xrw6                asia-southeast1-c  zone            100      pd-standard  READY
gke-standard-cluster-2-pvc-6b44acf3-ddd4-11e9-bc2e-42010a940047  asia-southeast1-c  zone            1        pd-standard  READY
gke-standard-cluster-2-pvc-f2b124d9-ddd3-11e9-bc2e-42010a940047  asia-southeast1-c  zone            1        pd-standard  READY
gke-standard-cluster-2-default-pool-ada9ad82-92qb                asia-southeast1-b  zone            100      pd-standard  READY
gke-standard-cluster-2-default-pool-ada9ad82-ptzn                asia-southeast1-b  zone            100      pd-standard  READY
gke-standard-cluster-2-pvc-5ed0bbd2-ddd4-11e9-bc2e-42010a940047  asia-southeast1-b  zone            1        pd-standard  READY
gke-standard-cluster-2-pvc-dfa8d014-ddd3-11e9-bc2e-42010a940047  asia-southeast1-b  zone            1        pd-standard  READY
gke-standard-cluster-1-pvc-0100ec4a-ddd1-11e9-b984-42010a940043  asia-southeast1-a  zone            1        pd-standard  READY
gke-standard-cluster-1-pvc-13c625c5-ddd1-11e9-b984-42010a940043  asia-southeast1-a  zone            1        pd-standard  READY
gke-standard-cluster-1-pvc-1fd9f51d-ddd1-11e9-b984-42010a940043  asia-southeast1-a  zone            1        pd-standard  READY
gke-standard-cluster-2-default-pool-eb7ab19a-rbf8                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-2-default-pool-eb7ab19a-z2h2                asia-southeast1-a  zone            100      pd-standard  READY
gke-standard-cluster-2-pvc-553c1902-ddd4-11e9-bc2e-42010a940047  asia-southeast1-a  zone            1        pd-standard  READY
gke-standard-cluster-2-pvc-cdabc75a-ddd3-11e9-bc2e-42010a940047  asia-southeast1-a  zone            1        pd-standard  READY
cloud_user_p_965beb@cloudshell:~$
```